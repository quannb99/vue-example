var eventBus = new Vue();

Vue.component("product", {
    props: {
        premium: {
            type: Boolean,
            required: true,
        },
    },
    template: `
    <div class="product">
        <div class="product-image">
            <img :title="message" :src="image" alt="">
        </div>
        <div class="product-info">
            <h1>{{ title }}</h1>
            <p v-if="inStock">In stock</p>
            <p v-else :class="{'out-of-stock': inStock == 0}">Out of stock</p>
            <p>Shipping: {{ shipping }}</p>
            <ul>
                <li v-for="(detail, index) in details" :key="index">{{ detail }}</li>
            </ul>
            <div v-for="(variant, index) in variants" :key="variant.variantId" class="color-box"
                :style="{ 'background-color': variant.variantColor }" @click="updateProduct(index)">
            </div>

            <button @click="addToCart" :disabled="!inStock" :class="{'disabledButton': !inStock}">Add to
                cart</button>

            <br><br>
            <product-tabs :reviews="reviews"></product-tabs>
            
        </div>
    </div>
    `,
    data() {
        return {
            message: "You opened this page on " + new Date().toLocaleString(),
            brand: "Vue",
            product: "Socks",
            selectedVariant: 0,
            inventory: 9,
            details: ["80% cotton", "20% polyester", "Gender-neutral"],
            variants: [{
                    variantId: 2234,
                    variantColor: "green",
                    variantImage: "images/green-socks.jpg",
                    vartiantQuantity: 10,
                },
                {
                    variantId: 2235,
                    variantColor: "blue",
                    variantImage: "images/blue-socks.jpg",
                    vartiantQuantity: 0,
                },
            ],
            reviews: [],
        };
    },
    methods: {
        addToCart: function () {
            this.$emit("add-to-cart", this.variants[this.selectedVariant].variantId);
        },
        removeFromCart: function () {
            this.$emit(
                "remove-from-cart",
                this.variants[this.selectedVariant].variantId
            );
        },
        updateProduct(selectedVariant) {
            this.selectedVariant = selectedVariant;
        },
    },
    computed: {
        title() {
            return this.brand + " " + this.product;
        },
        image() {
            return this.variants[this.selectedVariant].variantImage;
        },
        inStock() {
            return this.variants[this.selectedVariant].vartiantQuantity;
        },
        shipping() {
            if (this.premium) {
                return "Free";
            }
            return "$3";
        },
    },
    mounted() {
        eventBus.$on("review-submitted", (productReview) => {
            this.reviews.push(productReview);
        });
    },
});

Vue.component("product-review", {
    template: `
    <form class="review-form" @submit.prevent="onSubmit">

        <p v-if="errors.length">
            <b>Please correct the following error(s):</b>   
            <ul>
                <li v-for="error in errors">{{ error }}</li>
            </ul>
        </p>

        <p>
        <label for="name">Name:</label>
        <input id="name" v-model="name" placeholder="name">
        </p>
        
        <p>
        <label for="review">Review:</label>      
        <textarea id="review" v-model="review"></textarea>
        </p>
        
        <p>
        <label for="rating">Rating:</label>
        <select id="rating" v-model.number="rating">
            <option>5</option>
            <option>4</option>
            <option>3</option>
            <option>2</option>
            <option>1</option>
        </select>
        </p>
            
        <p>
        <input type="submit" value="Submit">  
        </p>    

    </form>
    `,
    data() {
        return {
            name: null,
            review: null,
            rating: null,
            errors: [],
        };
    },
    methods: {
        onSubmit() {
            this.errors = [];
            if (this.name && this.review && this.rating) {
                var productReview = {
                    name: this.name,
                    review: this.review,
                    rating: this.rating,
                };
                eventBus.$emit("review-submitted", productReview);
                this.name = null;
                this.review = null;
                this.rating = null;
            } else {
                if (!this.name) {
                    this.errors.push("Name required.");
                }
                if (!this.review) {
                    this.errors.push("Review required.");
                }
                if (!this.rating) {
                    this.errors.push("Rating required.");
                }
            }
        },
    },
});

Vue.component("product-tabs", {
    props: {
        reviews: {
            type: Array,
            required: true,
        },
    },
    template: `
        <div>
            <span class="tab"
                v-for="(tab, index) in tabs"    
                :key="index"
                :class="{ activeTab: selectedTab == tab }"
                @click="selectedTab = tab"
                >{{ tab }}
                </span>

            <div v-show="selectedTab == 'Reviews'">
                <p v-if="!reviews.length">There are no reviews yet.</p>
                <ul>
                    <li v-for="review in reviews">
                    <p>{{ review.name }}</p>
                    <p>Rating: {{ review.rating }}</p>
                    <p>{{ review.review }}</p>
                    </li>
                </ul>
            </div>

            <product-review v-show="selectedTab == 'Make a review'"></product-review>
        </div>
    `,
    data() {
        return {
            tabs: ["Reviews", "Make a review"],
            selectedTab: "Reviews",
        };
    },
});

var app = new Vue({
    el: "#app",
    data: {
        premium: true,
        cart: [],
    },
    methods: {
        updateCart(id) {
            this.cart.push(id);
        },
    },
});